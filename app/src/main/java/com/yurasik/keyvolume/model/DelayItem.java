package com.yurasik.keyvolume.model;

import android.database.Cursor;

import com.yurasik.keyvolume.db.DB;

/**
 * Created by NewUser on 9/20/16.
 */

public class DelayItem {
    private int setId;
    private int setRow;
    private int delay;

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_SET_ID = "set_id";
    public static final String COLUMN_SET_ROW = "set_row";
    public static final String COLUMN_TIMING_DELAY = "delay";

    public DelayItem() {
    }

    public DelayItem(int setId, int setRow, int delay) {
        this.setId = setId;
        this.setRow = setRow;
        this.delay = delay;
    }

    public int getSetId() {
        return setId;
    }

    public void setSetId(int setId) {
        this.setId = setId;
    }

    public int getSetRow() {
        return setRow;
    }

    public void setSetRow(int setRow) {
        this.setRow = setRow;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public String toString() {
        return "DelayItem{" +
                "setId=" + setId +
                ", setRow=" + setRow +
                ", delay=" + delay +
                '}';
    }

    public static DelayItem getItemFromCursor(int adapterPosition, Cursor cursor) {
        cursor.moveToPosition(adapterPosition);
        int setId = cursor.getInt(cursor.getColumnIndex(COLUMN_SET_ID));
        int setRow = cursor.getInt(cursor.getColumnIndex(COLUMN_SET_ROW));
        int delay = cursor.getInt(cursor.getColumnIndex(COLUMN_TIMING_DELAY));

        return new DelayItem(setId, setRow, delay);
    }
}
