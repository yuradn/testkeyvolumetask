package com.yurasik.keyvolume.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.yurasik.keyvolume.R;
import com.yurasik.keyvolume.common.Constants;
import com.yurasik.keyvolume.model.DelayItem;

import java.util.ArrayList;

import static com.yurasik.keyvolume.model.DelayItem.COLUMN_SET_ID;
import static com.yurasik.keyvolume.model.DelayItem.COLUMN_TIMING_DELAY;

/**
 * Created by NewUser on 9/20/16.
 */

public class DB {
    private final static String TAG = "My DB";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "timing_table";
    private static final String DB_NAME = "timing_db";

    private static final String DB_CREATE =
            "CREATE TABLE " + DB_TABLE + "( " +
                    DelayItem.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_SET_ID + " INTEGER, " +
                    DelayItem.COLUMN_SET_ROW + " INTEGER, " +
                    DelayItem.COLUMN_TIMING_DELAY + " INTEGER" +
                    ");";

    private final Context mCtx;

    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DB(Context ctx) {
        mCtx = ctx;
    }

    // open connection to DB
    public void open() {
        mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    // close connection to DB
    public void close() {
        if (mDBHelper!=null) mDBHelper.close();
    }

    // geta all date from DB_TABLE
    public Cursor getAllData() {
        return mDB.query(DB_TABLE, null, null, null, null, null, null);
    }

    // add record to DB_TABLE
    public void add(DelayItem delayItem) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_SET_ID, delayItem.getSetId());
        cv.put(DelayItem.COLUMN_SET_ROW, delayItem.getSetRow());
        cv.put(DelayItem.COLUMN_TIMING_DELAY, delayItem.getDelay());
        mDB.insert(DB_TABLE, null, cv);
    }

    public void addSetOfTiming(ArrayList<Integer> arrayList){
        Cursor mCount= mDB.rawQuery("SELECT * FROM " + DB_TABLE + " ORDER BY "
                + DelayItem.COLUMN_ID+ " DESC LIMIT 1;"
                , null);
        Log.d(TAG, DatabaseUtils.dumpCursorToString(mCount));
        mCount.moveToFirst();
        int count=0;
        if (mCount.getCount()>0) {
            count = mCount.getInt(mCount.getColumnIndex(DelayItem.COLUMN_SET_ID))+1;
        }
        mCount.close();
        DelayItem delayItem = new DelayItem();
        delayItem.setSetId(count);
        for (int i = 0; i < arrayList.size(); i++) {
            delayItem.setSetRow(i);
            delayItem.setDelay(arrayList.get(i));
            add(delayItem);
        }
    }

    public String[] getAverages(ArrayList<Integer> arrayList){
        String sets = Integer.toString(arrayList.get(0));
        for (int i = 1; i < arrayList.size(); i++) {
            sets=sets+", " + arrayList.get(i);
        }
        Cursor mCount= mDB.rawQuery("SELECT MIN(R1) AS RESULT FROM ( SELECT COUNT (*) AS R1, "
                + DelayItem.COLUMN_SET_ID + " FROM " + DB_TABLE + " WHERE "
                + DelayItem.COLUMN_SET_ID + " IN (" + sets + ")"
                + " GROUP BY "+DelayItem.COLUMN_SET_ID + ") TBL;", null);
        mCount.moveToFirst();
        int min = mCount.getInt(mCount.getColumnIndex("RESULT"));
        mCount.close();

        String[] mData = new String[arrayList.size()];
        for (int i = 0; i < mData.length; i++) {
            mData[i] = "";
        }

        mCount = mDB.rawQuery("SELECT * FROM " + DB_TABLE
        + " WHERE "
        + DelayItem.COLUMN_SET_ID + " IN (" + sets + ") "
        + "AND " + DelayItem.COLUMN_SET_ROW + " <" + min
                ,null);

        mCount.moveToFirst();
        int id = mCount.getInt(mCount.getColumnIndex(DelayItem.COLUMN_SET_ID));
        int index = 0;
        do {
            int tableIndex = mCount.getInt(mCount.getColumnIndex(DelayItem.COLUMN_SET_ID));
            if (tableIndex!=id) {
                id = tableIndex;
                index++;
            }
            int delay = mCount.getInt(mCount.getColumnIndex(DelayItem.COLUMN_TIMING_DELAY));
            if (mData[index].length()>0) {
                mData[index] += ",";
            }
            mData[index] += delay;
        } while (mCount.moveToNext());

        mCount.close();

        // query for Average
        String queryAverage = "SELECT "
                + "AVG("+DelayItem.COLUMN_TIMING_DELAY
                + ") FROM " + DB_TABLE
                + " WHERE " + DelayItem.COLUMN_SET_ID
                + " IN (" + sets + ") AND " + DelayItem.COLUMN_SET_ROW + " <" + min
                + " GROUP BY " + DelayItem.COLUMN_SET_ID;
        mCount = mDB.rawQuery(queryAverage, null);

        mCount.moveToFirst();
        for (int i = 0; i < mCount.getCount(); i++) {
            mData[i]+= " getAverages: "+mCount.getInt(0);
            mCount.moveToNext();
        }
        mCount.close();
        return mData;
    }

    // delete record from DB_TABLE
    public void delRec(int id) {
        mDB.delete(DB_TABLE, COLUMN_SET_ID + " = " + id, null);
    }

    // class of manage DB
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
