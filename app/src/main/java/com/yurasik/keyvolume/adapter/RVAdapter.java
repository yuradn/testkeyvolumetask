package com.yurasik.keyvolume.adapter;

import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yurasik.keyvolume.R;
import com.yurasik.keyvolume.interfaces.AdapterAction;
import com.yurasik.keyvolume.model.DelayItem;

import java.util.ArrayList;

/**
 * Created by NewUser on 9/19/16.
 */

public class RVAdapter extends RecyclerViewCursorAdapter<RVAdapter.TimingViewHolder>
        implements CompoundButton.OnCheckedChangeListener {
    private ArrayList<Integer> selectedSets;
    private AdapterAction adapterAction;

    public RVAdapter(Cursor cursor, AdapterAction adapterAction) {
        super(cursor);
        this.adapterAction = adapterAction;
    }

    @Override
    protected void onBindViewHolder(TimingViewHolder holder, Cursor cursor) {
        DelayItem delayItem = DelayItem
                .getItemFromCursor(holder.getAdapterPosition(), cursor);
        holder.checkBox.setOnCheckedChangeListener(null);

        boolean isTitle = false;
        if (holder.getAdapterPosition() == 0) {
            isTitle = true;
        } else {
            DelayItem prevItem = DelayItem.getItemFromCursor(holder.getAdapterPosition() - 1, cursor);
            isTitle = delayItem.getSetId() != prevItem.getSetId();
        }
        boolean checked = false;
        if (isTitle) {
            if (selectedSets != null && selectedSets.contains(Integer.valueOf(delayItem.getSetId()))) {
                checked = true;
            }
            holder.checkBox.setChecked(checked);

            holder.checkBox.setOnCheckedChangeListener(this);
            holder.checkBox.setTag("" + holder.getAdapterPosition());
        }
        holder.bind(delayItem, isTitle, checked);
    }

    @Override
    public TimingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_item, parent, false);
        return new TimingViewHolder(view);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (selectedSets == null) {
            selectedSets = new ArrayList<>();
        }
        int position = Integer.parseInt(compoundButton.getTag().toString());
        if (getCursor().isClosed()) throw new SQLException();
        int setId = DelayItem.getItemFromCursor(position, getCursor()).getSetId();
        if (b) {
            selectedSets.add(setId);
        } else {
            if (selectedSets.contains(setId)) {
                selectedSets.remove(Integer.valueOf(setId));
            }
        }
        adapterAction.actionButtonStatus(selectedSets.size() > 0);
    }

    public ArrayList<Integer> getSelectedSets() {
        return selectedSets;
    }

    public void setSelectedSets(ArrayList<Integer> selectedSets) {
        this.selectedSets = selectedSets;
    }

    public static class TimingViewHolder extends RecyclerView.ViewHolder {
        TextView tvId;
        TextView tvData;
        LinearLayout llContainer;
        CheckBox checkBox;

        public TimingViewHolder(View view) {
            super(view);
            tvId = TextView.class.cast(view.findViewById(R.id.tvId));
            tvData = TextView.class.cast(view.findViewById(R.id.tvData));
            llContainer = LinearLayout.class.cast(view.findViewById(R.id.llContainer));
            checkBox = CheckBox.class.cast(view.findViewById(R.id.checkbox));
        }

        public void bind(DelayItem delayItem, boolean isTitle, boolean isChecked) {
            checkBox.setChecked(isChecked);
            if (isTitle) {
                llContainer.setVisibility(View.VISIBLE);
                tvId.setText("" + (delayItem.getSetId()+1));
            } else {
                llContainer.setVisibility(View.GONE);
            }
            tvData.setText((delayItem.getSetRow()+1) + ": " + delayItem.getDelay());
        }
    }
}