package com.yurasik.keyvolume.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.yurasik.keyvolume.interfaces.AdapterAction;
import com.yurasik.keyvolume.R;
import com.yurasik.keyvolume.interfaces.TouchKeyAction;
import com.yurasik.keyvolume.adapter.RVAdapter;
import com.yurasik.keyvolume.common.Constants;
import com.yurasik.keyvolume.db.DB;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by NewUser on 9/16/16.
 */

public class MainFragment extends Fragment implements TouchKeyAction, LoaderManager.LoaderCallbacks<Cursor>
        , AdapterAction, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private Handler handler;
    private MyTimerRunnable myTimerRunnable;
    private long startingTime = -1;

    private ToggleButton tgAction;
    private ContentLoadingProgressBar progressBar;
    private TextView tvDelayTitle;
    private TextView tvDelay;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;
    private DB db;
    private Button btnDelete;
    private Button btnAverage;

    ArrayList<Integer> mCollection;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mCollection = new ArrayList<>();
        handler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = ContentLoadingProgressBar.class.cast(view.findViewById(R.id.progressBar));
        tvDelayTitle = TextView.class.cast(view.findViewById(R.id.tvDelayTitle));
        tvDelay = TextView.class.cast(view.findViewById(R.id.tvDelay));
        btnAverage = Button.class.cast(view.findViewById(R.id.btnAverage));
        btnDelete = Button.class.cast(view.findViewById(R.id.btnDelete));
        btnDelete.setOnClickListener(this);
        btnAverage.setOnClickListener(this);
        recyclerView = RecyclerView.class.cast(view.findViewById(R.id.recyclerView));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(false);
        tgAction = ToggleButton.class.cast(view.findViewById(R.id.tgAction));
        tgAction.setOnCheckedChangeListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = new DB(getContext());
        db.open();
        getLoaderManager().initLoader(Constants.LOADER_ID, null, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        db.close();
    }

    @Override
    public void actionPressedButton(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (!tgAction.isChecked()) {
                    return;
                }
                Integer delay = (int) (System.currentTimeMillis() - startingTime);
                stopTimerTask();
                mCollection.add(delay);
                startingTime = System.currentTimeMillis();
                tvDelay.setText(Integer.toString(delay));
                startTimerTask(TimeUnit.SECONDS.toMillis(Constants.DELAY_SECOND));
                break;
        }
    }

    private void showAverage(ArrayList<Integer> itemsId){
        ArrayList<Integer> mList = new ArrayList<>(itemsId);
        Collections.sort(mList);
        String[] times = db.getAverages(mList);
        for (int i = 0; i < mList.size(); i++) {
            times[i] = mList.get(i)+1+": "+times[i];
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        View customView = LayoutInflater.from(getContext()).inflate(R.layout.custom_dialog, null);
        alertDialog.setView(customView);
        alertDialog.setTitle(R.string.avarages);
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        ListView lv = (ListView) customView.findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1,times);
        lv.setAdapter(adapter);
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tgAction.isChecked()) {
            startTimerTask(TimeUnit.SECONDS.toMillis(Constants.DELAY_SECOND));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimerTask();
    }

    private void startTimerTask(long delay) {
        myTimerRunnable = new MyTimerRunnable();
        startingTime = System.currentTimeMillis();
        handler.postDelayed(myTimerRunnable
                , delay);
    }

    private void stopTimerTask() {
        if (handler != null && myTimerRunnable != null) {
            handler.removeCallbacks(myTimerRunnable);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case Constants.LOADER_ID:
                return new MyCursorLoader(getContext(), db);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (rvAdapter == null) {
            rvAdapter = new RVAdapter(data, this);
            recyclerView.setAdapter(rvAdapter);
            rvAdapter.changeCursor(data);
        } else {
            rvAdapter.changeCursor(data);
        }
        actionButtonStatus(rvAdapter.getSelectedSets() != null
                && rvAdapter.getSelectedSets().size() > 0);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        rvAdapter.swapCursor(null);
    }

    public void restartLoader() {
        Loader loader = getLoaderManager().getLoader(Constants.LOADER_ID);
        if (loader == null) {
            getLoaderManager().initLoader(Constants.LOADER_ID, null, this);
        } else {
            loader.forceLoad();
        }
    }

    public void deleteItem(ArrayList<Integer> itemsId) {
        for (int i = 0; i < itemsId.size(); i++) {
            db.delRec(itemsId.get(i));
        }
        restartLoader();
    }

    public void averageItem(ArrayList<Integer> itemsId) {
        showAverage(itemsId);
    }

    @Override
    public void actionButtonStatus(boolean status) {
        btnDelete.setEnabled(status);
        btnAverage.setEnabled(status);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDelete:
                deleteItem(rvAdapter.getSelectedSets());
                rvAdapter.setSelectedSets(null);
                break;
            case R.id.btnAverage:
                averageItem(rvAdapter.getSelectedSets());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            progressBar.show();
            tvDelay.setText("");
            tvDelay.setVisibility(View.VISIBLE);
            tvDelayTitle.setEnabled(true);
            startTimerTask(TimeUnit.SECONDS.toMillis(Constants.DELAY_SECOND));
        } else {
            progressBar.hide();
            tvDelay.setVisibility(View.INVISIBLE);
            tvDelayTitle.setEnabled(false);
            stopTimerTask();
        }
    }

    class MyTimerRunnable implements Runnable {
        @Override
        public void run() {
            if (mCollection.size() > 0) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvDelay.setText("");
                        Toast.makeText(getContext()
                                , "Add " + mCollection.size() + " items."
                                , Toast.LENGTH_SHORT).show();
                    }
                });
                db.addSetOfTiming(mCollection);
                mCollection.clear();
                restartLoader();

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "No items.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            vibrate();
            startTimerTask(TimeUnit.SECONDS.toMillis(Constants.DELAY_SECOND));
        }
    }

    private void vibrate() {
        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 150 milliseconds
        v.vibrate(150);
    }

    static class MyCursorLoader extends CursorLoader {
        DB db;

        public MyCursorLoader(Context context, DB db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllData();
            return cursor;
        }

    }

}