package com.yurasik.keyvolume.interfaces;

/**
 * Created by NewUser on 9/16/16.
 */

public interface TouchKeyAction {
    void actionPressedButton(int keyCode);
}
