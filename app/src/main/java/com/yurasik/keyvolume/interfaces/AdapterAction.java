package com.yurasik.keyvolume.interfaces;

/**
 * Created by NewUser on 9/21/16.
 */

public interface AdapterAction {
    void actionButtonStatus(boolean status);
}
