package com.yurasik.keyvolume.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.yurasik.keyvolume.fragment.MainFragment;
import com.yurasik.keyvolume.R;
import com.yurasik.keyvolume.interfaces.TouchKeyAction;

public class MainActivity extends AppCompatActivity {
    private TouchKeyAction touchKeyAction;
    private MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainFragment = (MainFragment) getSupportFragmentManager().
                findFragmentByTag(MainFragment.class.getCanonicalName());
        if (mainFragment==null) {
            mainFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_main, mainFragment, MainFragment.class.getCanonicalName())
                    .commitAllowingStateLoss();
        }
        touchKeyAction = mainFragment;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (touchKeyAction!=null) {
            touchKeyAction.actionPressedButton(keyCode);
            return true;
        } else {
            return false;
        }
    }


}